locals {
  len_certs = length(var.alternative-cert-names)
  domain_names = join(", ", var.alternative-cert-names)
}


resource "random_integer" "cert-suffix" {
  min = 1
  max = 100
  keepers = {
    # Generate a new integer each time we switch to a new listener ARN
    alternative_cert_names = local.domain_names
  }
}

## There are two different types of certificates here, one for a single name on the cert
## (this is the normal use case)
##
## And one which has an additional list of cert names, as specified using the variable
## "alternative-cert-names". The multi-name-cert object will put all the names in the
## list onto the cert.
resource "google_compute_managed_ssl_certificate" "single-name-cert" {
  count    = length(var.alternative-cert-names) == 0 ? 1 : 0
  provider = google-beta
  name     = "${var.backend_name}-cert"
  project  = var.project_id

  managed {
    domains = ["${var.backend_name}.${var.domain}."]
  }
}

resource "google_compute_managed_ssl_certificate" "multi-name-cert" {
  count    = length(var.alternative-cert-names) != 0 ? 1 : 0
  provider = google-beta
  name     = "${var.backend_name}-cert-${random_integer.cert-suffix.result}"
  project  = var.project_id

  lifecycle {
    create_before_destroy = true
  }

  managed {
    domains = concat(["${var.backend_name}.${var.domain}."], "${var.alternative-cert-names}")
  }
}
