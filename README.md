## Static Website Terraform

<!-- TOC -->

- [Static Website Terraform](#static-website-terraform)
- [Running The Helper Script](#running-the-helper-script)

<!-- /TOC -->


This repo contains the basic Terraform to create a Google Storage bucket and front it using Cloud CDN.

The terraform will create DNS entries for your bucket and will create a SSL cert containing all the names you have specified on the cert.

However, you must create the DNS entries for names on your cert that are not the domain you are running this from.

For example, I could have a domain `foo.blenderfox.net` which points to the bucket, but I may also want to have the name `foo.blenderfox.com` on the bucket. The Google Managed SSL which this Terraform uses will allow that, but the cert will only provision successfully if there is a CNAME record for `foo.blenderfox.com` that points to `foo.blenderfox.net`. You can add this using whatever DNS provider is applicable for you. Route 53, Cloud DNS, etc.

To run this terraform, modify the variables in `terraformPlanAndRun.sh` and `terraformDestroy.sh`:


* `TERRAFORM_BIN` -- This is the path to your terraform binary. Intended in case you have multiple version of Terraform on your system (like I do)

* `PROJECT_ID` -- GCP Project ID

* `MANAGED_ZONE` -- GCP Cloud DNS Managed Zone

* `DOMAIN` -- The domain name to use to assemble DNS names

* `CREDENTIALS_JSON` -- Name of your GCP credentials JSON

* `EXTRA_CERT_NAMES` -- This is commented out as it is intended to overridden at command line. See ["Running The Helper Script"](#running-the-helper-script) section below

* `NAME_PREFIX` -- The prefix to put at the start of the naming

* `ENV` -- The environment category. For example, "test", "dev", "prod"


## Running The Helper Script

The helper script will init and plan the Terraform but will not run it until you press a key to continue

It resides in the script `terraformPlanAndRun.sh`

You must override the variable `EXTRA_CERT_NAMES` on running:

`EXTRA_CERT_NAMES="[]" ./terraformPlanAndRun.sh`

This will create a stack with a cert with a cert of only one name.

To create extra names in the cert, add additional names to the list:

`EXTRA_CERT_NAMES="[\"extra-domain-name1\",\"extra-domain-name-1\"]" ./terraformPlanAndRun.sh`

However, due to the way the Terraform has created the resources, it does not seem to be possible to "update"

To Destroy the stack, use the `terraformDestroy.sh` script in the same way -- you do not need to pass anything into the cert list as Terraform will use the state file

`EXTRA_CERT_NAMES="[]" ./terraformDestroy.sh`
