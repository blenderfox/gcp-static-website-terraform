provider "google" {
  credentials = file(var.gcloud_credentials_file)
  project     = var.project_id
  region      = var.location
}

provider "google-beta" {
  credentials = file(var.gcloud_credentials_file)
  project     = var.project_id
  region      = var.location
}
