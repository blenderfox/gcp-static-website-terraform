TERRAFORM_BIN=/usr/local/bin/terraform-0.12.28

PROJECT_ID=REPLACE_ME
MANAGED_ZONE=REPLACE_ME
DOMAIN=REPLACE_ME
CREDENTIALS_JSON=REPLACE_ME
#EXTRA_CERT_NAMES=[]
NAME_PREFIX=REPLACE_ME

ENV="test"

$TERRAFORM_BIN init

$TERRAFORM_BIN destroy \
  -var="bucket_name=$NAME_PREFIX-$ENV" \
  -var="backend_name=$NAME_PREFIX-$ENV" \
  -var="alternative-cert-names=$EXTRA_CERT_NAMES" \
  -var="bucket_labels={\"name\": \"static-website-$ENV\",\"project\":\"static-website-$ENV\",\"team\": \"dev\",\"env\":\"$ENV\",\"creator\" : \"blenderfox\",\"support\" : \"24-7\"}" \
  -var="project_id=$PROJECT_ID" \
  -var="managed_zone=$MANAGED_ZONE" \
  -var="domain=$DOMAIN" \
  -var="gcloud_credentials_file=$CREDENTIALS_JSON" \
  -var="env=$ENV"
