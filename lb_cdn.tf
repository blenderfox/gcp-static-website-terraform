#Backend bucket and backend service
resource "google_compute_backend_bucket" "static_bucket_backend" {
  name        = "${var.backend_name}-${var.env}-backend-bucket"
  project     = var.project_id
  bucket_name = google_storage_bucket.static_bucket.name
  enable_cdn  = true
}

#Configuring the load balancing service
resource "google_compute_url_map" "urlmap" {
  name        = "${var.backend_name}-${var.env}-url-map"
  project     = var.project_id
  description = "a description"

  default_service = google_compute_backend_bucket.static_bucket_backend.self_link

  host_rule {
    hosts        = ["${var.backend_name}.${var.domain}"]
    path_matcher = "allpaths"
  }

  path_matcher {
    name            = "allpaths"
    default_service = google_compute_backend_bucket.static_bucket_backend.self_link

    path_rule {
      paths   = ["/*"]
      service = google_compute_backend_bucket.static_bucket_backend.self_link
    }
  }

  test {
    service = google_compute_backend_bucket.static_bucket_backend.self_link
    host    = "${var.backend_name}.${var.domain}"
    path    = "/*"
  }
}

## These are split out because the dependent cert must be created first before
## this proxy can be created. And on deletion, this resource must be deleted
## before the cert can be deleted. Terraform seems unable to realise that, and
## often borks
resource "google_compute_target_https_proxy" "single-name-cert-proxy" {
  count            = length(var.alternative-cert-names) == 0 ? 1 : 0
  name             = "${var.backend_name}-static-website-proxy-single"
  project          = var.project_id
  url_map          = google_compute_url_map.urlmap.self_link
  ssl_certificates = [google_compute_managed_ssl_certificate.single-name-cert.0.self_link]
}

resource "google_compute_target_https_proxy" "multi-name-cert-proxy" {
  count            = length(var.alternative-cert-names) != 0 ? 1 : 0
  name             = "${var.backend_name}-static-website-proxy-multiple"
  project          = var.project_id
  url_map          = google_compute_url_map.urlmap.self_link
  ssl_certificates = [google_compute_managed_ssl_certificate.multi-name-cert.0.self_link]
}

## Similarly with the front end configuration
resource "google_compute_global_forwarding_rule" "single-name-cert-frontend_configuration" {
  count      = length(var.alternative-cert-names) == 0 ? 1 : 0
  name       = "${var.backend_name}-${var.env}-forwarding-rule"
  project    = var.project_id
  ip_address = google_compute_global_address.external_ip.address
  port_range = "443"
  target     = google_compute_target_https_proxy.single-name-cert-proxy[0].self_link
}

resource "google_compute_global_forwarding_rule" "multi-name-cert-frontend_configuration" {
  count      = length(var.alternative-cert-names) != 0 ? 1 : 0
  name       = "${var.backend_name}-${var.env}-forwarding-rule"
  project    = var.project_id
  ip_address = google_compute_global_address.external_ip.address
  port_range = "443"
  target     = google_compute_target_https_proxy.multi-name-cert-proxy[0].self_link
}
