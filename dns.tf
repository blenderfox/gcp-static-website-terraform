#Configuring services needed by load balancing service, global static external ip

resource "google_compute_global_address" "external_ip" {
  project = var.project_id
  name    = "${var.backend_name}-externalstaticip"
}

#Adding record set to managed zone
resource "google_dns_record_set" "website" {
  project = var.project_id
  name    = "${var.backend_name}.${var.domain}."
  type    = "A"
  ttl     = 300

  managed_zone = var.managed_zone

  rrdatas = [google_compute_global_address.external_ip.address]
}
