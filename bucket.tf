resource "google_storage_bucket" "static_bucket" {
  name          = var.bucket_name
  location      = var.bucket_location
  project       = var.project_id
  storage_class = var.bucket_storage_class

  ## force_destroy means when the bucket is to be deleted, it will automatically
  ## delete all the contents within the bucket. If this isn't set or is set to
  ## false, Terraform will fail on deleting the bucket, if there are files in the
  ## bucket.
  force_destroy = true

  versioning {
    enabled = var.bucket_versioning
  }

  website {
    main_page_suffix = var.main_page_suffix
    not_found_page   = var.not_found_page
  }

  labels = var.bucket_labels
}

resource "google_storage_default_object_acl" "default_obj_acl" {
  bucket      = google_storage_bucket.static_bucket.name
  role_entity = var.role_entity
}
