output "self_link" {
  value = google_storage_bucket.static_bucket.self_link
}

output "url" {
  value = google_storage_bucket.static_bucket.url
}
