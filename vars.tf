variable "bucket_name" {
}

variable "backend_name" {
}

variable "project_id" {
}

variable "managed_zone" {
}

#Labels for bucket
variable "bucket_labels" {
  type = map(string)

}

variable "domain" {
}

variable "gcloud_credentials_file" {
}

variable "env" {
}

variable "bucket_location" {
  default     = "EU"
}

variable "location" {
  default = "europe-west2"
}

variable "bucket_storage_class" {
  default     = "MULTI_REGIONAL"
}

variable "bucket_versioning" {
  default     = false
}

variable "main_page_suffix" {
  description = "bucket's directory index where missing objects are treated as potential directories"
  default     = "index.html"
}

variable "not_found_page" {
  description = "The custom object to return when a requested resource is not found"
  default     = "404.html"
}

# Storage bucket default object ACLs variables
variable "role_entity" {
  description = "Sets bucket default object ACLs to allow all users read access to objects"
  type        = list(string)
  default     = ["READER:allUsers"]
}

## Specify additional names on the cert. Override on cli
variable "alternative-cert-names" {
  type    = list(string)
  default = []
}
